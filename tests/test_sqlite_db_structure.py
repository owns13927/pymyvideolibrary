"""
Elias Wood (owns13927@yahoo.com)
2015-04-24
unit test for if it's works to have view of calc view of base tables...
"""
import sys
if '..' not in sys.path: sys.path.append('..')
import sqlite3 as sql
from tests.testbase import TestBase
#TODO: complete me
# test lazy loading
# test fn isn't called unless the row is called for!


#===============================================================================
# Test for a good sqlite structre
#===============================================================================
class Test_SQLiteStructures(TestBase):
    def test_if_slow(self):
        db_file = self.get_new_file_name('aaa.db')
        
        # create the db
        db = sql.connect(db_file,detect_types=sql.PARSE_DECLTYPES|sql.PARSE_COLNAMES)
        
        # create long sql fn
        def long_fn(v):
            import time
            print v,'...'
            time.sleep(3)
            print 'done sleeping'
            return reversed(v)
        
        #--- make a dummy structure
        # base table
        with db as conn:
            conn.executescript('''
            CREATE TABLE my_base
            (
                id INT AUTOINCREMENT PRIMARY KEY,
                title TEXT,
                release_date timestamp
            );
            
            CREATE VIEW my_calc_view AS
            SELECT
                b.id as id,
                b.title as title,
                b.title || ' (' || year(b.release_date) || ')' as name,
                year(b.release_date) as "year",
                long_fn(b.title) as rev_title
            FROM my_base b;
            ''')
        
        
#===============================================================================
# Run Test
#===============================================================================
def run_test():
    import os; os.chdir('..')
    import unittest
    suite = unittest.TestLoader().loadTestsFromTestCase(Test_SQLiteStructures)
    unittest.TextTestRunner().run(suite)

#===============================================================================
# Main
#===============================================================================
if __name__ == '__main__':
    #run_test()
    import tempfile
    import os.path
    import datetime as dt
    import random
    
    # generate new unique folder and filename
    db_dir = tempfile.mkdtemp()
    db_file = os.path.join(db_dir,'aaa.db')
        
    # create the db
    db = sql.connect(db_file,detect_types=sql.PARSE_DECLTYPES|sql.PARSE_COLNAMES)
    
    # create long sql fn
    def long_fn(v):
        import time
        time.sleep(random.uniform(1,2))
        print 'done sleeping',v,type(v)
        return ''.join(reversed(v))
    db.create_function('rev', 1, long_fn)
    
    #--- make a dummy structure
    # base table
    with db as conn:
        conn.executescript('''
        CREATE TABLE my_base
        (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            title TEXT,
            release_date timestamp
        );
        
        CREATE VIEW my_calc_view AS
        SELECT
            b.id as id,
            b.title as title,
            b.title || ' (' || strftime('%Y',b.release_date) || ')' as name,
            strftime('%Y',b.release_date) as "year",
            rev(b.title) as rev_title
        FROM my_base b;
        ''')
    
    def pick_some(s,a,b):
        return 
    
    s = 'abcdefghijklmnopqrstuvwxyz '
    n = '123456789'
    with db as conn:
        for i in xrange(1000):
            conn.execute('insert into my_base (title,release_date) values (?,?);',
                         (''.join(random.choice(s) for i in xrange(random.randint(3,10))),
                          dt.datetime(random.randint(1992,2015),random.randint(1,12),random.randint(1,28))
                         )
                         )
    
    
    s = ''
    while True:
        s += raw_input('>')
        if s[:2] == r'\q':
            break
        if sql.complete_statement(s):
            try:
                with db as conn:
                    a = conn.execute(s)
                    for row in a:
                        for col in row:
                            if isinstance(col,dt.datetime):
                                print '{:<10}'.format(col.strftime('%Y-%m-%d')),
                            else:
                                print '{:<10}'.format(col),
                        print
            except Exception as e: print 'error {!r}'.format(e)
            finally: s = ''
    
    try: os.remove(db_file)
    except: pass
    try: os.rmdir(db_dir)
    except: pass
    
    
    